import React,{ useEffect, useState } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'



import MainHome from './src/Screens/Drawer'
import AuthScreen from './src/Screens/Auth'
import SplashScreen from './src/Screens/Splash'

const Stack = createStackNavigator()

function App() {

  const [splashLoading, setSplashLoading] = useState(true)
  const [loggedIn, setLoggedIn] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setSplashLoading(false)
    }, 2000)
  })

  if(splashLoading) {
    return (
      <SplashScreen />
    )
  }

  return (
    <NavigationContainer>
          <Stack.Navigator 
            screenOptions={{
              headerShown: false
            }}>
              
             {
               loggedIn === true ? (
                  <Stack.Screen name="Home" component={MainHome} />
               ): (
                <Stack.Screen name="Auth" component={AuthScreen}
                  initialParams={{loginFx: setLoggedIn}}
                  options={{
                    title: 'Sign in',
                    animationTypeForReplace: 'pop'
                  }}
                />
               )
             }

            </Stack.Navigator>
    </NavigationContainer>


  )
}

export default App
