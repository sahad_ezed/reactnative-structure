import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
function Login(props) {
    const { loginFx } = props.route.params
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Login Page</Text>
            <TouchableOpacity onPress={() => loginFx(true)}>
                <Text>Next</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Login
