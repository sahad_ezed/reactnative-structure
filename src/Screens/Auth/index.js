import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import Login from './Login'

const Stack = createStackNavigator()

function AuthScreen(props) {
  const { loginFx } = props.route.params
  return (
        <Stack.Navigator>
            <Stack.Screen 
                name="Login" 
                component={Login} 
                initialParams={{loginFx}}
            />
        </Stack.Navigator>
  )
}

export default AuthScreen
