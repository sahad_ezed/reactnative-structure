import React from 'react'
import { Text, Button } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'

import Home from './Home'
import ProductList from './ProductList'
import Cart from './Cart'

const Stack = createStackNavigator()

function HomeScreen() {
  return (
        <Stack.Navigator>
            <Stack.Screen 
                name="Home" 
                component={Home} 
                options={{
                    headerTitle: props => <Text>Sahad</Text>,
                  }}
            />

            <Stack.Screen 
                name="Products" 
                component={ProductList} 
            />

            <Stack.Screen 
                name="Cart" 
                component={Cart} 
            />
        </Stack.Navigator>
  )
}

export default HomeScreen
