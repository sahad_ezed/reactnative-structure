import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
function OrderList({navigation}) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>OrderList Page</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Detailed Order')}>
                <Text>Detailed Order Page</Text>
            </TouchableOpacity>
        </View>
    )
}

export default OrderList
