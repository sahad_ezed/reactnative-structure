import React from 'react'
import { View, Text } from 'react-native'

function SplashScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Splash Screen</Text>
        </View>
    )
}

export default SplashScreen
